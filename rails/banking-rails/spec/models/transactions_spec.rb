require 'rails_helper'

RSpec.describe Transaction, type: :model do
  let (:transaction) { described_class.new account: create(:account) }
  
  context 'when amount is not given' do
    it 'should not be valid' do
      expect(transaction).not_to be_valid
    end
  end
  
  context 'when amount is zero' do
    let (:transaction) { described_class.new amount: 0, account: create(:account) }

    it 'should not be valid' do
      expect(transaction).not_to be_valid
    end
  end
  
  context 'when amount is positive' do
    let (:transaction) { described_class.new amount: 500, account: create(:account) }

    it 'should be valid' do
      expect(transaction).to be_valid
    end
  end
  
  context 'when amount is negative' do
    let (:transaction) { described_class.new amount: -100, account: create(:account, balance: 200) }
    
    it 'should be valid' do
      expect(transaction).to be_valid
    end
  end
end