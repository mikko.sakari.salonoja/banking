require 'rails_helper'

RSpec.describe Account, type: :model do
  let (:account) { described_class.new }
    
  context 'when name is not given' do
    it 'should not be valid' do
      expect(account).not_to be_valid
    end
  end
    
  context 'when name is given and balance is not' do
    let (:account) { described_class.new name: 'Account 1' }
      
    it 'should be valid' do
      expect(account).to be_valid
    end
      
    it 'should set balance to zero' do
      expect(account.balance).to eq(0)
    end
  end
    
  context 'when given balance is negative' do
    let (:account) { described_class.new name: 'Account 1', balance: -500 }
      
    it 'should not be valid' do
      expect(account).not_to be_valid
    end
  end
end