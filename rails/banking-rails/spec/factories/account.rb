FactoryBot.define do
  factory :account do
    sequence :name do |n|
      "MyAccount - #{n}"
    end
    balance  { 0 }
  end
end