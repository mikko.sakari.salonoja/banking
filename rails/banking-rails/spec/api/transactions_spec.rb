require 'rails_helper'
describe "transactions", :type => :request do
  let (:model) { Transaction }
  let (:path) { '/transactions.json' }
  let (:account) { create(:account) }
  
  describe 'create' do
    it 'returns the amount and id and updates account balance for deposit' do
      balance_before = 0

      amount = 30
      transaction_type = 'deposit'
      post path, params: { amount: amount.to_s, transaction_type: transaction_type, account_id: account.id.to_s}

      expect(JSON.parse(response.body)['amount']).to eq(amount)
      expect(JSON.parse(response.body)['id']).to eq(model.last.id)
      account.reload
      expect(account.balance).to eq(amount)
    end
    
    it 'returns the amount and id and updates account balance for withdraw' do
      balance_before = 100
      model.create account: account, amount: balance_before

      amount = 30
      transaction_type = 'withdraw'
      post path, params: { amount: amount, transaction_type: transaction_type, account_id: account.id.to_s}

      expect(JSON.parse(response.body)['amount']).to eq(-1 * amount)
      expect(JSON.parse(response.body)['id']).to eq(model.last.id)
      account.reload
      expect(account.balance).to eq(balance_before - amount)
    end
    
    context 'invalid parameters returning 405 status code' do
      it 'has does not have an account reference' do
        post path, params: {amount: '100', transaction_type: 'deposit'}
    
        expect(response.status).to eq(405)
      end
      
      it 'has empty account reference' do
        post path, params: {amount: '100', transaction_type: 'deposit', account_id: '1234567890'} # Non-existing id
    
        expect(response.status).to eq(405)
      end

      it 'has does not have amount' do
        post path, params: {transaction_type: 'deposit', account_id: account.id}
    
        expect(response.status).to eq(405)
      end
      
      it 'has zero amount' do
        post path, params: {amount: '0', transaction_type: 'deposit', account_id: account.id}
    
        expect(response.status).to eq(405)
      end
      
      it 'has too high amount for withdraw' do
        post path, params: {amount: '50', transaction_type: 'withdraw', account_id: account.id}
    
        expect(response.status).to eq(405)
      end
      
      it 'has does not have transaction_type' do
        post path, params: {amount: 100, account_id: account.id}
    
        expect(response.status).to eq(405)
      end
      
      it 'has unsupported transaction_type' do
        post path, params: {amount: 100, transaction_type: 'play', account_id: 1234567890} # Non-existing id
    
        expect(response.status).to eq(405)
      end
    end
  end
  
  describe 'index' do
    it 'returns the account transactions with balance after info' do
      model.create account: account, amount: 100
      model.create account: account, amount: -30

      get path, params: { account_id: account.id.to_s}

      expect(JSON.parse(response.body)[0]['amount']).to eq(100)
      expect(JSON.parse(response.body)[0]['balance_after']).to eq(100)
      expect(JSON.parse(response.body)[1]['amount']).to eq(-30)
      expect(JSON.parse(response.body)[1]['balance_after']).to eq(70)
    end
    
    it 'returns 405 status if account_id parameter is pointing to emptiness' do
      get path, params: { account_id: '1234567890'} # Non-existing id
    
      expect(response.status).to eq(405)
    end
  end
end