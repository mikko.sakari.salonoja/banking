require 'rails_helper'
describe "accounts", :type => :request do
  let (:model) { Account}
  let (:path) { '/accounts.json' }
  
  context 'required parameters are not given' do
    it 'returns 405 status if name parameter is missing' do
      post path
    
      expect(response.status).to eq(405)
    end
  end
  
  it 'returns the name and id' do
    name = 'Account 1'
    post path, params: { name: name}

    expect(JSON.parse(response.body)['name']).to eq(name)
    expect(JSON.parse(response.body)['id']).to eq(model.last.id)
  end
end
