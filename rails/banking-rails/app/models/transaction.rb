class Transaction < ApplicationRecord
  belongs_to :account
  validates :amount, numericality: { other_than: 0 }, presence: true
  validates :account, presence: true
  
  validate do |record|
    if record.account && record.amount && record.account.balance + record.amount < 0
      record.errors[:amount] << 'Too high withdraw'
    end
  end
  
  after_create do
    self.account.update balance: self.account.balance + self.amount
  end
  
  def self.for_api(items)
    items.map.with_index do |item, index| 
      { account_id: item.account_id,
        amount: item.amount,
        created_at: item.created_at,
        balance_after: items[0, index+1].map(&:amount).reduce(0, :+) }
    end
  end
end
