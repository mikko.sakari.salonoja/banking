class Account < ApplicationRecord
  has_many :transactions
  validates :name, presence: true
  validates :balance, numericality: { greater_than_or_equal_to: 0 }
  
  after_initialize do
    self.balance ||= 0
  end
end
