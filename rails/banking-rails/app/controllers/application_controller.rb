class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session # In order to allow API requests
end
