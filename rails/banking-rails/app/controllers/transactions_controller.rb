class TransactionsController < ApplicationController
  before_action :account, only: [:create, :index]
  before_action :transaction, only: [:create]

  def create
    @item.save
  end
  
  def index
    @items = Transaction.for_api @account.transactions
  end
  
  # Helper functions
  
  def account
    @account = Account.find_by_id(params[:account_id].to_i)
    head 405 unless (@account && @account.valid?)
  end
  
  def transaction
    @item = Transaction.new account: @account, amount: amount_for_transaction_type(params[:amount], params[:transaction_type])
    head 405 unless @item.valid?
  end
  
  def amount_for_transaction_type(amount, transaction_type)
    if !amount.blank? && !transaction_type.blank? && ['deposit','withdraw'].include?(transaction_type)
      amount = amount.to_i
      amount *= -1 if transaction_type=='withdraw'
      amount
    end
  end
end
