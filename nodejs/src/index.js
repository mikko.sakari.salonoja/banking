const express = require('express');
const cors = require('cors');
const app = express();
const db = require('./persistence');
const createAccount = require('./routes/createAccount');
const listTransactions = require('./routes/listTransactions');
const createTransaction = require('./routes/createTransaction');

app.use(express.urlencoded());
app.use(express.static(__dirname + '/static'));

// TODO: this CORS support activation for Swagger Try it out functionality is not working.
const corsArg = cors();
app.post('/accounts', corsArg, createAccount);
app.get('/transactions', corsArg, listTransactions);
app.post('/transactions', corsArg, createTransaction);

db.init().then(() => {
    app.listen(3000, () => console.log('Listening on port 3000'));
}).catch((err) => {
    console.error(err);
    process.exit(1);
});

const gracefulShutdown = () => {
    db.teardown()
        .catch(() => {})
        .then(() => process.exit());
};

process.on('SIGINT', gracefulShutdown);
process.on('SIGTERM', gracefulShutdown);
process.on('SIGUSR2', gracefulShutdown); // Sent by nodemon