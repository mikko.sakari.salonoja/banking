const db = require('../persistence');

module.exports = async (req, res) => {
    // TODO: validate input.
    // - Required: account_id
    var transactions = await db.listTransactions({id: req.query.account_id});
    // Calculate balance after each transaction. TODO: do this on model level because skinnier controllers are better.
    transactions = transactions.map((item,index,items)=>{
	  var new_item = {created_at: item.created_at, amount: item.amount};
      var amounts_for_balance_after = items.slice(0,index+1).map((slice_item)=>slice_item.amount);
      new_item.balance_after = amounts_for_balance_after.reduce((a, b) => a + b, 0); // Sum array items
      return new_item;
    });
    res.send(transactions);
};