const db = require('../persistence');

module.exports = async (req, res) => {
    // TODO: validate input
    // - Required: name
    const name = req.body.name;

    const account = await db.createAccount(name);
    res.send(account);
};