const db = require('../persistence');

module.exports = async (req, res) => {
    // TODO: validate input
    // - Required: account_id
    // - transaction_type must deposit or withdraw
    // - account balance must be higher than withdraw

    const account = await db.getAccount(req.body.account_id);
    const amount = req.body.amount * (req.body.transaction_type === 'withdraw' ? -1 : 1);

    db.updateAccountBalance(account.balance + amount);
    const transaction = await db.createTransaction(account, amount);
    res.send(transaction);
};