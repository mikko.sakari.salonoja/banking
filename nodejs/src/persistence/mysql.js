const waitPort = require('wait-port');
const fs = require('fs');
const mysql = require('mysql');

const {
    MYSQL_HOST: HOST,
    MYSQL_HOST_FILE: HOST_FILE,
    MYSQL_USER: USER,
    MYSQL_USER_FILE: USER_FILE,
    MYSQL_PASSWORD: PASSWORD,
    MYSQL_PASSWORD_FILE: PASSWORD_FILE,
    MYSQL_DB: DB,
    MYSQL_DB_FILE: DB_FILE,
} = process.env;

let pool;

async function init() {
    const host = HOST_FILE ? fs.readFileSync(HOST_FILE) : HOST;
    const user = USER_FILE ? fs.readFileSync(USER_FILE) : USER;
    const password = PASSWORD_FILE ? fs.readFileSync(PASSWORD_FILE) : PASSWORD;
    const database = DB_FILE ? fs.readFileSync(DB_FILE) : DB;

    await waitPort({ host, port : 3306, timeout: 15000 });

    pool = mysql.createPool({
        connectionLimit: 5,
        host,
        user,
        password,
        database,
    });

    return new Promise((acc, rej) => {
	    pool.query(
            'CREATE TABLE IF NOT EXISTS accounts (id int NOT NULL AUTO_INCREMENT, name varchar(255), balance int, created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)',
            err => {
                if (err) return rej(err);

                console.log(`Connected to mysql db at host ${HOST}`);
                acc();
            }
        );
        pool.query(
            'CREATE TABLE IF NOT EXISTS transactions (id NOT NULL AUTO_INCREMENT, account_id int, amount int, created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)',
            err => {
                if (err) return rej(err);

                console.log(`Connected to mysql db at host ${HOST}`);
                acc();
            }
        );
    });
}

async function teardown() {
    return new Promise((acc, rej) => {
        pool.end(err => {
            if (err) rej(err);
            else acc();
        });
    });
}

async function createAccount(name) {
    return new Promise((acc, rej) => {
        pool.query(
            'INSERT INTO accounts (name, balance) VALUES (?, ?)',
            [name, 0],
            err => {
                if (err) return rej(err);
                acc();
            }
        );
    });
}

async function getAccount(account_id) {
    return new Promise((acc, rej) => {
        pool.query('SELECT * FROM accounts WHERE rowid = ?', [account_id], 
            (err,rows) => {
                if (err) return rej(err);
                var account = rows[0];
                account.id = account_id;
                acc(account);
            }
        );
    });
}

async function updateAccountBalance(account_id, balance) {
    return new Promise((acc, rej) => {
        pool.query(
            'UPDATE accounts SET balance=? WHERE rowid=?',
            [balance, account_id],
            err => {
                if (err) return rej(err);
                acc();
            }
        );
    });
}

async function listTransactions(account) {
    return new Promise((acc, rej) => {
        pool.query('SELECT * FROM transactions WHERE account_id = ?', [account.id], 
            err => {
                if (err) return rej(err);
                acc();
            }
        );
    });
}

async function createTransaction(account, amount) {
    return new Promise((acc, rej) => {
        pool.query(
            'INSERT INTO transactions (account_id, amount) VALUES (?, ?)',
            [account.id, amount],
            err => {
                if (err) return rej(err);
                acc();
            }
        );
    });
}

module.exports = {
    init,
    teardown,
    createAccount,
    getAccount,
    updateAccountBalance,
    listTransactions,
    createTransaction
};
