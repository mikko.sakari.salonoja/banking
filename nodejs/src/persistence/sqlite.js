const sqlite3 = require('sqlite3').verbose();
const fs = require('fs');
const location = process.env.SQLITE_DB_LOCATION || '/etc/banking/banking.db';

let db, dbAll, dbRun;

function init() {
    const dirName = require('path').dirname(location);
    if (!fs.existsSync(dirName)) {
        fs.mkdirSync(dirName, { recursive: true });
    }

    return new Promise((acc, rej) => {
        db = new sqlite3.Database(location, err => {
            if (err) return rej(err);

            if (process.env.NODE_ENV !== 'test')
                console.log(`Using sqlite database at ${location}`);

	        db.run(
                'CREATE TABLE IF NOT EXISTS accounts (name varchar(255), balance int, created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)',
                err => {
                  if (err) return rej(err);
                  acc();
                }
            );

            db.run(
                'CREATE TABLE IF NOT EXISTS transactions (account_id int, amount int, created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)',
                err => {
                   if (err) return rej(err);
                   acc();
                }
            );
        });
    });
}

async function teardown() {
    return new Promise((acc, rej) => {
        db.close(err => {
            if (err) rej(err);
            else acc();
        });
    });
}

async function createAccount(name) {
    return new Promise((acc, rej) => {
         var statement = db.prepare('INSERT INTO accounts (name, balance) VALUES (?, ?)');
         statement.run(name, 0,
            err => {
                if (err) return rej(err);
                const account = {id: statement.lastID, name: name};
                acc(account);
            }
        );
    });
}

async function getAccount(account_id) {
    return new Promise((acc, rej) => {
        db.all('SELECT * FROM accounts WHERE rowid=?', [account_id], 
            (err,rows) => {
                if (err) return rej(err);
                var account = rows[0];
                account.id = account_id;
                acc(account);
            }
        );
    });
}

async function updateAccountBalance(account_id, balance) {
    return new Promise((acc, rej) => {
         var statement = db.prepare('UPDATE accounts SET balance=? WHERE rowid=?');
         statement.run(balance, account_id,
            err => {
                if (err) return rej(err);
                const account = {id: statement.lastID, balance: balance};
                acc(account);
            }
        );
    });
}

async function listTransactions(account) {
    return new Promise((acc, rej) => {
        db.all('SELECT * FROM transactions WHERE account_id = ?', [account.id], 
            (err,rows) => {
                if (err) return rej(err);
                acc(rows);
            }
        );
    });
} 

async function createTransaction(account, amount) {
    return new Promise((acc, rej) => {
         var statement = db.prepare('INSERT INTO transactions (account_id, amount) VALUES (?, ?)');
          statement.run(account.id, amount,
            err => {
                if (err) return rej(err);
                const transaction = {id: statement.lastID, amount: amount};
                acc(transaction);
            });
    });
}

module.exports = {
    init,
    teardown,
    createAccount,
    getAccount,
    updateAccountBalance,
    listTransactions,
    createTransaction
};