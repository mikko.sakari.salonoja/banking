const db = require('../../src/persistence/sqlite');
const fs = require('fs');

beforeEach(() => {
    if (fs.existsSync('/etc/banking/banking.db')) {
        fs.unlinkSync('/etc/banking/banking.db');
    }
});

test('it initializes correctly', async () => {
    await db.init();
});

test('it can create an account', async () => {
    await db.init();

    const account = await db.createAccount('MyAccount');
    expect(account.name).toEqual('MyAccount');
});

test('it can update account balance', async () => {
    await db.init();

    var account = await db.createAccount('MyAccount');
    await db.createTransaction(account, 100);
    account = await db.updateAccountBalance(account.id, 100);

    expect(account.balance).toEqual(100);
});

test('it can deposit, withdraw and list transactions', async () => {
    await db.init();

    var account = await db.createAccount('MyAccount');
    await db.createTransaction(account, 100);
    await db.createTransaction(account, -10);

    const transactions = await db.listTransactions(account);

    expect(transactions.length).toEqual(2);
    expect(transactions[0].amount).toEqual(100);
    expect(transactions[1].amount).toEqual(-10);
});
