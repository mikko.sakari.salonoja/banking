const db = require('../../src/persistence');
const listTransactions = require('../../src/routes/listTransactions');

jest.mock('../../src/persistence', () => ({
    listTransactions: jest.fn()
}));

test('it lists the transactions corrects correctly', async () => {
	const transactions = [{created_at: '2020-01-01', amount: 100}, {created_at: '2020-02-02', amount: -10}];
    db.listTransactions.mockReturnValue(Promise.resolve(transactions));

    const req = { body: { id: 1 } };
    const res = { send: jest.fn() };

    await listTransactions(req, res);
    expect(db.listTransactions.mock.calls.length).toBe(1);
    expect(res.send.mock.calls[0][0][0].amount).toEqual(100);
    // TODO: check balance after
});