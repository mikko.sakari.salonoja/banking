const db = require('../../src/persistence');
const createTransaction = require('../../src/routes/createTransaction');

jest.mock('../../src/persistence', () => ({
    getAccount: jest.fn(),
    updateAccountBalance: jest.fn(),
    createTransaction: jest.fn()
}));

test('it create a transaction correctly', async () => {
    const name = 'MyAccount';
    db.getAccount.mockReturnValue(Promise.resolve({id: 1, name: name, balance: 100}));

    const req = { body: { id: 1, transaction_type: 'withdraw', amount: 30 } };
    const res = { send: jest.fn() };

    await createTransaction(req, res);
    expect(db.createTransaction.mock.calls.length).toBe(1);
    expect(db.updateAccountBalance.mock.calls[0][0]).toBe(70);
    expect(db.createTransaction.mock.calls[0][0].id).toBe(1); // Account ID check
    expect(db.createTransaction.mock.calls[0][1]).toBe(-30); // Amount check
});