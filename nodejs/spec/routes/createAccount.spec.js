const db = require('../../src/persistence');
const createAccount = require('../../src/routes/createAccount');

jest.mock('../../src/persistence', () => ({
    createAccount: jest.fn()
}));

test('it stores the account correctly', async () => {
    const name = 'MyAccount';
    db.createAccount.mockReturnValue(Promise.resolve({id: 1, name: name}));

    const req = { body: { name: name } };
    const res = { send: jest.fn() };

    await createAccount(req, res);
    expect(db.createAccount.mock.calls.length).toBe(1);
    expect(res.send.mock.calls[0][0].name).toEqual(name);
});
