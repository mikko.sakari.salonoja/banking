# Banking example

Install Docker

## NodeJS

### Build
- cd nodejs
- docker build -t banking-nodejs .
- docker run -dp 7000:3000 banking-nodejs

### Create account
curl -X POST -d name=MyAccount http://localhost:7000/accounts

### Deposit cash
curl -X POST -d account_id=1 -d transaction_type=deposit -d amount=100 http://localhost:7000/transactions

### Withdraw cash
curl -X POST -d account_id=1 -d transaction_type=withdraw -d amount=10 http://localhost:7000/transactions

### Transaction list for an account
curl -X GET "http://localhost:7000/transactions?account_id=1"

### Download Swagger documentation
- Navigate to http://localhost:7000
- Paste Swagger doc to editor.swagger.io
- Open endpoint on the right frame and click Try it out
- Fill in parameters and click execute
- Response from the API is shown in the page. This works out of the box in Chrome. With other browsers browser settings need tuning (e.g. Safari).

## Ruby on Rails - Development
- cd rails
- docker-compose build
- docker-compose run --rm --service-ports ruby_dev
- cd banking-rails
- bundle update && bundle install
- rake db:migrate
- rails server -p $PORT -b 0.0.0.0



See API usage instructions on Swagger by changing the port in [nodejs application Swagger file](https://gitlab.com/mikko.sakari.salonoja/banking/-/blob/master/nodejs/src/static/swagger.yml) from 7000 to 3000 